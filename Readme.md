Flask based docker registry UI
------------------------------


To build the docker image
------------------------
docker build -t "registry-ui:latest" .


To run the container
--------------------
For insecure registry:

docker run -d --restart always --name registry_ui  -p 80:80 -e REGISTRY_IP=10.10.0.135 -e REGISTRY_PORT=5000 registry-ui:latest


For secure registry:

docker run -d --restart always --name registry_ui  -p 80:80 -e REGISTRY_IP=10.10.0.135 -e REGISTRY_PORT=5000 -e SECURE_USER=<username> -e SECURE_PASSWORD=<password> registry-ui:latest

<username> and <password> are registry user credentials used to query the registry.
