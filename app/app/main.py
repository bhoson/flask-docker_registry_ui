from flask import Flask
from flask import render_template
import requests
import os

app = Flask(__name__)
REGISTRY_IP = os.environ["REGISTRY_IP"]
REGISTRY_PORT = os.environ["REGISTRY_PORT"]
try:
  SECURE_USER = os.environ["SECURE_USER"]
  SECURE_PASSWORD = os.environ["SECURE_PASSWORD"]
except KeyError:
  print "[WARNING] User ID and password not provided. Cannot access secure registry."
  SECURE_USER = ""
  SECURE_PASSWORD = ""

@app.route("/")
def home():
  banner = "Snehadeep B."
  catalog_response = None
  tag_response = None
  unsorted_repo = None
  repo = None
  # Fetch catalog of images
  if SECURE_USER == SECURE_PASSWORD == "":
    catalog_response = requests.get("http://"+REGISTRY_IP+":"+REGISTRY_PORT+"/v2/_catalog")
  else:
    catalog_response = requests.get("https://"+SECURE_USER+":"+SECURE_PASSWORD+"@"+REGISTRY_IP+":"+REGISTRY_PORT+"/v2/_catalog", verify=False)
  image_list = catalog_response.json()
  unsorted_repo = {key: "" for key in image_list['repositories']}

  # Fetch tag list
  api_url_beg = "http://"+REGISTRY_IP+":"+REGISTRY_PORT+"/v2/"
  api_url_beg_ssl = "https://"+SECURE_USER+":"+SECURE_PASSWORD+"@"+REGISTRY_IP+":"+REGISTRY_PORT+"/v2/"
  api_url_trail = "/tags/list"
  for img in unsorted_repo.keys():
    if SECURE_USER == SECURE_PASSWORD == "":
      tag_response = requests.get(api_url_beg+img+api_url_trail).json()
    else:
      tag_response = requests.get(api_url_beg_ssl+img+api_url_trail, verify=False).json()
    try:
      unsorted_repo[img] = " | ".join(tag_response['tags']).encode('ascii','ignore')
    except KeyError:
      unsorted_repo[img] = "No valid tags available"
     
    repo = sorted(unsorted_repo.items())
  return render_template('home.html', repo=repo,banner=banner)

if __name__ == "__main__":
  # Only for debugging while developing
  app.run(host='0.0.0.0', debug=True, port=80)
